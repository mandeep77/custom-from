import React, { Component } from "react";
import "./form.css";
import video from "./back.mp4"
// import Password from "../components/password";

export class Form extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       password:false
    }
  }
  

  submitHandler=(e)=>{
    
    
    e.preventDefault();
    const formData = {};
    for (const field in this.refs) {
      formData[field] = this.refs[field].value;
        console.log(formData[field]);
        if(field==='password'){
          console.log('here')
          console.log(formData[field])
          var minNumberofChars = 6;
          var maxNumberofChars = 16;
          var regularExpression  = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;
          if(formData[field].length < minNumberofChars || formData[field].length > maxNumberofChars){
              console.log('wronggggg')
              this.setState({
                password:true
              })
          }
         
          else if(!regularExpression.test(formData[field])) {
           
              // alert("password should contain atleast one number and one special character");
              this.setState({
                password:true
              })
             console.log('wornggg')
          }
          else{
       
            this.setState({
              password:false
            },console.log('here------>',this.state.password))
          }
        }
    }
    console.log('-->', formData);
  }
  render() {
    const { state } = this.props.location;
    const body = state.map((feild) => {
      if (feild.label === "select") {
        return (
          <div className="box">
            <p>{feild.title}</p>

            <select>
              {feild.option.map((option) => {
                return <option value={option}>{option}</option>;
              })}
            </select>
          </div>
        );
      }
      if (feild.label === "radio") {
        return (
          <div>
           
            <p>{feild.title}</p>
            <div className="radiodiv">
              {feild.option.map((option) => {
                return (
                  <div className="radio">
                    <label>{option}</label>
                    <input
                      type="radio"
                      name={feild.title}
                      value={option}
                    ></input>
                  </div>
                );
              })}
            </div>
          </div>
        );
      }
      if(this.state.password && feild.label==='password'){
        return(
          <div>
           <p>password</p>
            <input ref={feild.type} type="password" name="password" />
           {this.state.password && <small>password should contain min 6 char, one upcase,lowercase and number</small>} 
          </div>
        )
        
      }
     
        return (
          <div>
            <p>{feild.label}</p>
            <input ref={feild.type} type={feild.type} name={feild.name} />
          </div>
        );
      
     

      

   
    });
    return (
      <div className="main">
         <video autoPlay loop muted id="myVideo">
            <source src={video} type="video/mp4" />
            </video>
      <div className="container">
        <form onSubmit={this.submitHandler} className="main-form">
          <h1>Generated Form</h1>
          {body}
          {console.log("hello")}
          <button onSubmit={this.submitHandler} className="submit-btn">Submit</button>
        </form>
      </div>
      </div>
    );
  }
}

export default Form;
