import React, { Component } from "react";
import "./custom.css";
import Radio from "./radio.js"

export class Custom extends Component {
  constructor(props) {
    super(props);

    this.state = {
      feildName: "",
      title:"",
      selectValue: "text",
      value: [],
    };
  }

  handleChange = (e) => {
    this.setState({ selectValue: e.target.value }, () =>
      console.log(this.state.selectValue)
    );
  };
  handleChangeText = (e) => {
    
    this.setState({ feildName: e.target.value }, () =>
      console.log(this.state.selectValue)
    );
  };

  handleChangeTextDrop = (e)=>{
    this.setState({ title: e.target.value }, () =>
    console.log(this.state.selectValue)
  );
  }

  select = (value) => {
    this.setState(
      (pre) => ({
        value: [...pre.value, ...value.split(",")],
      }),
      () => console.log(this.state.value)
    )
    
  };

  render() {
    return (
      <div className="box1">
        <input
          onChange={this.handleChangeText}
          type="text"
          placeholder="Enter the name"
        ></input>
        <select
          defaultValue={this.state.selectValue}
          onChange={this.handleChange}
        >
          <option value="password">password</option>
          <option value="Number">Number</option>
          <option value="email">email</option>
          <option value="text">text</option>
          <option value="Textarea">textarea</option>
          <option value="select">drop down</option>
          <option value="radio">radio</option>
        </select>
        {this.state.selectValue === "select" && (
          <input
            onChange={this.handleChangeTextDrop}
            type="text"
            placeholder="Enter the name for drop down"
          ></input>
        )}
        {this.state.selectValue === "select" && (
          <input
            onChange={this.handleChangeText}
            type="text"
            placeholder="Enter the select option"
          ></input>
        )}
        {this.state.selectValue === "select" && (
          <button onClick={() => this.select(this.state.feildName)}>
            add option
          </button>
        )}
        {this.state.selectValue === "select" && 
          <button
          onClick={() =>
            this.props.addFields('select', this.state.value,this.state.title)
          }
        >
          add dropdown
        </button>
        }
         {this.state.selectValue === "radio" && <Radio  add={this.props.addFields}/>}
       {this.state.selectValue==="password" &&  <button onClick={() =>
            this.props.addFields(this.state.feildName, this.state.selectValue)
          }>
          add
        </button>}
        {this.state.selectValue==="Number" &&  <button onClick={() =>
            this.props.addFields(this.state.feildName, this.state.selectValue)
          }>
          add
        </button>}
        {this.state.selectValue==="email" &&  <button onClick={() =>
            this.props.addFields(this.state.feildName, this.state.selectValue)
          }>
          add
        </button>}
        {this.state.selectValue==="Textarea" &&  <button onClick={() =>
            this.props.addFields(this.state.feildName, this.state.selectValue)
          }>
          add
        </button>}
        {this.state.selectValue==="text" &&  <button onClick={() =>
            this.props.addFields(this.state.feildName, this.state.selectValue)
          }>
          add
        </button>}
      </div>
    );
  }
}

export default Custom;
