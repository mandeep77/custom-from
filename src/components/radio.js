import React, { Component } from 'react'

export class Radio extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            fieldName:"",
             name:"",
             values:[]
        }
    }

    handleNameRadio = (e)=>{
        this.setState({ name: e.target.value }
      );
      }
      handleNameRadioval = (e) => {
    
        this.setState({ fieldName: e.target.value },()=>{console.log(this.state.fieldName)})
      };
    
      select = (value) => {
        this.setState(
          (pre) => ({
            
            values: [...pre.values, ...value.split(",")],

          }),
          () => console.log(this.state.values)
        )
        
      };
    
    render() {
        return (
            <div>
                <input onChange={this.handleNameRadio} type='text' placeholder="name"></input>
                <input  onChange={this.handleNameRadioval} type='text' placeholder="values"></input>
                <button onClick={()=>this.select(this.state.fieldName)}>Add Radio</button>
                <button onClick={()=>this.props.add('radio',this.state.values,this.state.name)}>Add To Form</button>
            </div>
        )
    }
}

export default Radio
