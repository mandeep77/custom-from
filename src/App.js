import React, { Component } from "react";
import Name from "./components/name";
import Email from "./components/email";
import Password from "./components/password";
import Custom from "./components/custom";
import "./App.css";
import Form from "./pages/form";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";

export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      button: "",
      fields: [],
    };
  }
  addFields = (label, type, title) => {
    if (label === "select") {
      this.setState((pre) => ({
        fields: [
          ...pre.fields,
          {
            label: label,
            option: type,
            title: title,
          },
        ],
      }));
    } 
    else if(label==="radio"){
      this.setState((pre) => ({
        fields: [
          ...pre.fields,
          {
            label: label,
            option: type,
            title: title,
          },
        ],
      }),()=>console.log(this.state.fields)); 
    }
    else {
      this.setState((pre) => ({
        fields: [
          ...pre.fields,
          {
            label: label,
            type: type,
          },
        ],
      }));
    }
  };

  render() {
    const body = this.state.fields.map((feild) => {
      if (feild.label === "select") {
        return (
          <div className="box">
            <p>{feild.title}</p>

            <select>
              {feild.option.map((option) => {
                return <option value={option}>{option}</option>;
              })}
            </select>
          </div>
        );
      }
      if (feild.label === "radio") {
        return (
          <div >
            <p>{feild.title}</p>
            <div className="radiodiv">
            {feild.option.map((option) => {
              return <div className="radio">
                <label>{option}</label>
                 <input  type='radio'name={feild.title} value={option} ></input>
               </div>
               
              })}
               </div>
          </div>
        );
      }
      return (
        <div>
          <p>{feild.label}</p>
          <input type={feild.type} name={feild.name} />
        </div>
      );
    });

    return (
      <Router>
        <Route path="/form" exact component={Form}>
          {/* <Form  /> */}
        </Route>
        <Route path="/" exact component={Form}>
          <div>
            <div className="buttonDiv">
              <button
                className="button button1"
                onClick={() => this.setState({ button: "name" })}
              >
                name
              </button>
              <button
                className="button button2"
                onClick={() => this.setState({ button: "email" })}
              >
                EMAIL
              </button>
              <button
                className="button button3"
                onClick={() => this.setState({ button: "password" })}
              >
                PASSWORD
              </button>
              <button
                className="button button3"
                onClick={() => this.setState({ button: "custom" })}
              >
                CUSTOM FIELD
              </button>
            </div>

            <div class="freUsed">
              {this.state.button === "name" && (
                <Name addFields={this.addFields} />
              )}
              {this.state.button === "email" && (
                <Email addFields={this.addFields} />
              )}
              {this.state.button === "password" && (
                <Password addFields={this.addFields} />
              )}
              {this.state.button === "custom" && (
                <Custom addFields={this.addFields} />
              )}
              <div className="mainFrom">
                {body}
                {console.log(this.state.fields)}
              </div>
            </div>
          </div>
          <div className="generate">
          <Link className="link"
          to={{
            pathname: "/form",
            state: this.state.fields, // your data array of objects
          }}
        >
          Generate From
        </Link>
        </div>
        </Route>
       
      </Router>
    );
  }
}

export default App;
